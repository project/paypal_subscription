
Overview of paypal_subscription.module
--------------------------------------

The paypal_subscription module works together with the paypal_framework
module. The system allows a site visitor to pay money and receive a
special role on your Drupal site. As a member of this role, you may give
the subscriber special privileges.

Example: An "authorized user" cannot access your site's archive. But by
purchasing a subscription, the visitor is promoted to "subscriber" role
which has access to the site archives.

NOTICE: This module requires cron. Other modules may be required in
order to enforce access controls. See http://drupal.org/project/releases
for more modules.

Installation 
------------

1) Enable Instant Payment Notifications for your PayPal account. Enter
   http://www.example.com/paypal/ipn as the URL at the PayPal site.

   NOTICE: To use PayPal's IPN service, you must have either a PayPal
   Premier or Business account. If you have a Personal account, follow
   Paypal's intructions for upgrading.

       http://www.paypal.com/cgi-bin/webscr?cmd=_help-ext&eloc=135

2) This module requires the paypal_framework module. The framework
   module must be installed and configured with at least one PayPal 
   email address. To configure this module, go to:

       administer -> settings -> paypal_framework

3) You are now ready to install the paypal_subscription module. Place
   the file paypal_subscription.module in your Drupal module directory.

4) Create the database table by doing something equivalent to this:

       mysql -u drupal -p drupal < optin.mysql

   You may also choose to use phpMyAdmin. Go to the SQL section and
   click on the "Browse" button, and find the paypal_subscription.mysql
   file on your computer. Follow phpMyAdmin's instructions to run the
   SQL script.

5) To enable this module, go to:

       administer -> modules

6) Using PayPal's web site, create a PayPal button. The button may be
   for donations or "buy now".  You may also use a
   simple HTML link to your PayPal order page.

   NOTICE: Some people have reported problems with shopping cart
   buttons.  This issue is under investigation.

   You may also choose to use PayPal's subscription feature in
   combination with this module. PayPal's subscription feature will bill
   your subscribers automatically, and this subscription module for
   Drupal will make sure that your subscribers are granted a special
   role.

   NOTICE: Be sure to use an item number! This system works on
   item_number recognition. If there is a problem in this area then the
   purchaser will not be promoted to the choosen role.

7) Using your Drupal web site, create a subscription. Enter the
   item_number you chose in step 6. To create a subscription, go to:

       administer -> PayPal -> subscriptions

   If you decide to use PayPal's subscription feature, you may not want
   to send emails to your subscribers that warn of the impending end of
   their subscription. Warning emails would be unnecessary and confusing
   because PayPal will ensure that your subscribers pay you on time. To
   disable this feature, uncheck the box labeled "Send warning emails".

8) Place the PayPal links or buttons on your Drupal site. When the user
   clicks on the button or link they will be guided through the purchasing
   process by the Paypal system. Upon returning to your Drupal web site,
   they will have the special role that you sold to them.

9) The subscription module is dependant on the Drupal cron.php running
   at regular intervals to demote non-payers from the role sold. It is
   recommended that you check with your hosting service to make sure
   that cron is available and working. If you cannot use cron, then you
   may want to investigate the following options:

       a) Drupal's poormanscron.module 
       b) A cron service such as provided by www.fastcron.com 
          (commercial) or www.webcron.org (free) 
       c) Manually visit http://www.example.com/cron.php on your Drupal 
          web site.

Author 
------

Nic Ivy (nji@njivy.org)
